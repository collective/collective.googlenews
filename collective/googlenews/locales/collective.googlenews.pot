# --- PLEASE EDIT THE LINES BELOW CORRECTLY ---
# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: collective.googlenews\n"
"POT-Creation-Date: 2013-01-24 10:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: en\n"
"Language-Name: English\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: collective.googlenews\n"

#: collective/googlenews/controlpanel.py:12
msgid "Google News Settings"
msgstr ""

#: collective/googlenews/interfaces.py:28
msgid "Keywords Mapping"
msgstr ""

#: collective/googlenews/interfaces.py:22
msgid "Portal types"
msgstr ""

#: Profile description in configure.zcml
msgid "Removes support for Google News from your site."
msgstr ""

#: collective/googlenews/interfaces.py:23
msgid "Select portal types you want to apply digit id."
msgstr ""

#: Profile title in configure.zcml
msgid "Support for Google News"
msgstr ""

#: Profile description in configure.zcml
msgid "To include a site in Google News you need to comply with certain technical requirements. This package provides support for this."
msgstr ""

#: Profile title in configure.zcml
msgid "collective.googlenews uninstall"
msgstr ""

#. Default: "You can configure the settings of collective.googlenews add-on"
#: collective/googlenews/controlpanel.py:13
msgid "controlpanel_desc"
msgstr ""

#: collective/googlenews/interfaces.py:30
msgid "keyword|googlekeyword"
msgstr ""

