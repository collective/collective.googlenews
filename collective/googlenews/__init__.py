# -*- coding: utf-8 -*-

from zope.i18nmessageid import MessageFactory

from collective.googlenews.config import PROJECTNAME

_ = MessageFactory(PROJECTNAME)
